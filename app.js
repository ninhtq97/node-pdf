const express = require('express');
const bodyParse = require('body-parser');
const fs = require('fs');
const path = require('path');
const PDF = require('pdfkit');

const app = express();

app.use(bodyParse.urlencoded({ extended: true }));

app.use(express.static(path.join(__dirname, 'public')));

const pathStatic = './static';

fs.exists(pathStatic, res => {
  if (!res) {
    fs.mkdir(pathStatic, err => {
      if (err) return console.log(err);
    });
  }
  return true;
});

const randomFolder = Math.random()
  .toString(36)
  .substring(7);

fs.exists(`${pathStatic}/${randomFolder}`, res => {
  if (!res) {
    fs.mkdir(`${pathStatic}/${randomFolder}`, err => {
      if (err) return console.log(err);

      const doc = new PDF();

      doc.pipe(
        fs.createWriteStream(`${pathStatic}/${randomFolder}/contract.pdf`)
      );

      doc.text('Hello', 20, 20);

      doc.addPage();

      doc.fontSize(25).text('ASDJHAKJSDH');
      doc.end();

      const doc2 = new PDF();

      doc2.pipe(
        fs.createWriteStream(`${pathStatic}/${randomFolder}/receipt.pdf`)
      );
      doc2.text('Oh no');

      doc2.end();
    });
  }
  return true;
});

const port = process.env.PORT || 5858;
app.listen(port, () =>
  console.log(`Server running on http://localhost:${port}`)
);
